export const pageCreator = (arr, cont, num) => {
  let index = num * 10 - 10;

  while (index < num * 10) {
    cont.insertAdjacentHTML(
      "beforeend",
      `<div class="comment">
      <p>
        <span data-name="id">${arr[index].id}</span><br>
        <span data-name="name">${arr[index].name}</span><br>
        <a data-name="email" href="mailto:${arr[index].email}">${arr[index].email}</a><br>
        <span data-name="body">${arr[index].body}</span>
      </p>
    </div>`
    );
    index++;
  }
};

export const createBtn = (arr, parent) => {
  let maxNum = Math.ceil(arr.length / 10);
  let count = 1;

  while (count <= maxNum) {
    let button = `<button class="btn">${count}</button>`;
    parent.insertAdjacentHTML("beforeend", button);
    count++;
  }
};

export const btnClick = (data, btnArr, cntnr) => {
  btnArr.forEach((el) => {
    el.addEventListener("click", (e) => {
      let btnNum = parseInt(e.target.innerText);
      cntnr.replaceChildren();
      pageCreator(data, cntnr, btnNum);
    });
  });
};
