import { pageCreator, createBtn, btnClick } from "./functions.js";

const req = new XMLHttpRequest();
const url = "https://jsonplaceholder.typicode.com/comments";
let commentBase = [];

const container = document.querySelector(".container");
const btnsBlock = document.querySelector(".buttons-block");

req.open("GET", url);
req.send();
req.addEventListener("readystatechange", () => {
  if (req.readyState === 4 && req.status >= 200 && req.status < 300) {
    commentBase = JSON.parse(req.responseText);
    if (!Array.isArray(commentBase)) {
      throw new Error("З сервера прийшов не масив!");
    }
    pageCreator(commentBase, container, 1);
    createBtn(commentBase, btnsBlock);

    const [...btn] = document.querySelectorAll(".btn");
    btnClick(commentBase, btn, container);
  } else if (req.readyState === 4) {
    throw new Error("Помилка у запиті!");
  }
});
